//
//  IntentViewController.swift
//  FordPassIntentUI
//
//  Created by Gary Riches on 29/06/2018.
//  Copyright © 2018 Bouncing Ball Limited. All rights reserved.
//

import IntentsUI
import UIKit

// As an example, this extension's Info.plist has been configured to handle interactions for INSendMessageIntent.
// You will want to replace this or add other intents as appropriate.
// The intents whose interactions you wish to handle must be declared in the extension's Info.plist.

// You can test this example integration by saying things to Siri like:
// "Send a message using <myApp>"

class IntentViewController: UIViewController, INUIHostedViewControlling {
	
    var variableHeight: CGFloat = 240
    
	let SharedDefaults = UserDefaults.init(suiteName: "group.mobi.bouncingball.fordpass")!
    let serviceView = ServiceView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
        
    // MARK: - INUIHostedViewControlling
    
    // Prepare your view controller for the interaction to handle.
    func configureView(for parameters: Set<INParameter>, of interaction: INInteraction, interactiveBehavior: INUIInteractiveBehavior, context: INUIHostedViewContext, completion: @escaping (Bool, Set<INParameter>, CGSize) -> Void) {
        // Do configuration here, including preparing views and calculating a desired size for presentation.
        if interaction.intent is ServiceCheckIntent {
            configureUI()
            drawFord()
        }
        
        if let intent = interaction.intent as? ExtendReservationIntent {
            view.addSubview(loadExtendReservationView(for: intent))
            variableHeight = 360
            drawFord()
        }
        
        if let intent = interaction.intent as? QueryReservationIntent {
            view.addSubview(loadQueryReservationView(for: intent))
            variableHeight = 360
            drawFord()
        }
        
        completion(true, parameters, desiredSize(with: variableHeight))
    }
    
    func desiredSize(with height: CGFloat) -> CGSize {
        return CGSize(width: self.extensionContext!.hostedViewMaximumAllowedSize.width, height: height)
    }
    
    var desiredWidth: CGFloat {
        return self.extensionContext!.hostedViewMaximumAllowedSize.width
    }
    
    var desiredHeight: CGFloat {
        return self.extensionContext!.hostedViewMaximumAllowedSize.height
    }

    
    func loadServiceCheckView() -> UIView {
        return UIView()
    }
    
    func loadExtendReservationView(for intent: ExtendReservationIntent) -> UIView {
        let extendView = Bundle.main.loadNibNamed("ExtendReservationNib", owner: self, options: nil)![0] as? ExtendReservationView
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        
        let hours = (intent.hours?.intValue ?? 0) / 2
        let minutes = (intent.minutes?.intValue ?? 0) / 2
        var endDate = Date()
        if let date = SharedDefaults.value(forKey: "reservationDate") as? Date {
            endDate = date.addingTimeInterval(Double(hours * 60 * 60) + Double(minutes * 60))
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .short
        }
        
        extendView?.fromLabel.text = dateFormatter.string(from: (SharedDefaults.value(forKey: "reservationStartDate") as? Date ?? Date()))
        extendView?.toLabel.text = dateFormatter.string(from: endDate)
        extendView!.frame = CGRect(x: 0, y: 0, width: desiredWidth, height: 360)
        return extendView!
    }
    
    func loadQueryReservationView(for intent: QueryReservationIntent) -> UIView {
        let extendView = Bundle.main.loadNibNamed("ExtendReservationNib", owner: self, options: nil)![0] as? ExtendReservationView
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        var endDate = Date()
        if let date = SharedDefaults.value(forKey: "reservationDate") as? Date {
            endDate = date
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .short
        }
        
        extendView?.fromLabel.text = dateFormatter.string(from: (SharedDefaults.value(forKey: "reservationStartDate") as? Date ?? Date()))
        extendView?.toLabel.text = dateFormatter.string(from: endDate)
        extendView!.frame = CGRect(x: 0, y: 0, width: desiredWidth, height: 360)
        return extendView!
    }
    
    func configureUI() {
        serviceView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(serviceView)
        setupConstraints()
    }
    
    private func setupConstraints() {
        let constraints:[NSLayoutConstraint] = [
            serviceView.topAnchor.constraint(equalTo: view.topAnchor),
            serviceView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            serviceView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            serviceView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }
    
    private func drawFord() {
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
        
        let constraints: [NSLayoutConstraint] = [
            containerView.topAnchor.constraint(equalTo: self.view.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
        
        containerView.backgroundColor = UIColor(red:0.00, green:0.18, blue:0.42, alpha:1.0)
        containerView.addSubview(FordLetterView(frame: CGRect(x: desiredWidth / 2 - variableHeight / 2, y: 0,
                                                              width: variableHeight,
                                                              height: variableHeight), {
                                                                UIView.animate(withDuration: 1, delay: 0.4, options: .curveEaseInOut, animations: {
                                                                    containerView.center = CGPoint(x: containerView.center.x, y: containerView.center.y)
                                                                    containerView.alpha = 0
                                                                    containerView.transform = CGAffineTransform(scaleX: 50.0, y: 50.0)
                                                                }) { _ in
                                                                    containerView.removeFromSuperview()
                                                                }
        }))
    }
}
