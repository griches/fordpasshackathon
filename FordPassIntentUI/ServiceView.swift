//
//  ServiceView.swift
//  FordPassIntentUI
//
//  Created by DevPair24 on 11/07/2018.
//  Copyright © 2018 Bouncing Ball Limited. All rights reserved.
//

import UIKit

class ServiceView: UIView {
    
    var title: UILabel!
    var imageView: UIImageView!
    var leftLabel: UILabel!
    var rightLabel: UILabel!
    var leftLabel2: UILabel!
    var rightLabel2: UILabel!
    var bottomLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        title = UILabel()
        title.text = "Service due in 8,200 miles"
        title.textAlignment = .center
        title.font = UIFont.boldSystemFont(ofSize: 20.0)
        
        let image = UIImage(named: "fordVan.jpeg")
        imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit

        leftLabel = UILabel()
        leftLabel.text = "Service Due In:"
        leftLabel.textAlignment = .left
        leftLabel.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.thin)
        leftLabel2 = UILabel()
        leftLabel2.text = "Total Mileage:"
        leftLabel2.textAlignment = .left
        leftLabel2.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.thin)
        
        rightLabel = UILabel()
        rightLabel.text = "8,200 Miles"
        rightLabel.textAlignment = .left
        rightLabel.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.thin)
        rightLabel2 = UILabel()
        rightLabel2.text = "810,030 Miles"
        rightLabel2.textAlignment = .left
        rightLabel2.font = UIFont.systemFont(ofSize: 12, weight: UIFont.Weight.thin)

        bottomLabel = UILabel()
        bottomLabel.text = "Time for a new Ford? "
        bottomLabel.textAlignment = .center
        bottomLabel.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.thin)
        
        addSubview(title)
        addSubview(imageView)
        addSubview(leftLabel)
        addSubview(leftLabel2)
        addSubview(rightLabel)
        addSubview(rightLabel2)
        addSubview(bottomLabel)
        
        subviews.forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
        
        let constraints:[NSLayoutConstraint] = [
            title.topAnchor.constraint(equalTo: topAnchor, constant: 10.0),
            title.leadingAnchor.constraint(equalTo: leadingAnchor),
            title.trailingAnchor.constraint(equalTo: trailingAnchor),
            imageView.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 10.0),
            imageView.heightAnchor.constraint(equalToConstant: 100.0),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor),
            leftLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10.0),
            leftLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 50.0),
            leftLabel.trailingAnchor.constraint(equalTo: centerXAnchor),
            rightLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10.0),
            rightLabel.leadingAnchor.constraint(equalTo: centerXAnchor),
            rightLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            leftLabel2.topAnchor.constraint(equalTo: leftLabel.bottomAnchor, constant: 10.0),
            leftLabel2.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 50.0),
            leftLabel2.trailingAnchor.constraint(equalTo: centerXAnchor),
            rightLabel2.topAnchor.constraint(equalTo: rightLabel.bottomAnchor, constant: 10.0),
            rightLabel2.leadingAnchor.constraint(equalTo: centerXAnchor),
            rightLabel2.trailingAnchor.constraint(equalTo: trailingAnchor),
            bottomLabel.topAnchor.constraint(equalTo: leftLabel2.bottomAnchor, constant: 15.0),
            bottomLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            bottomLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        ]
        NSLayoutConstraint.activate(constraints)
    }

}
