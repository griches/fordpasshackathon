//
//  FordLetterView.swift
//  FordLetterF
//
//  Created by Mike Gopsill on 13/04/2018.
//  Copyright © 2018 Mike Gopsill. All rights reserved.
//

import UIKit
import CoreGraphics

enum FordLetterViewConstant: CGFloat {
    case length = 414
}

class FordLetterView: UIView {
    
    private var completed: (() -> Void)?
    private var firstDuration = 3.0
    
    init(frame: CGRect, _ completed: (() -> Void)? = nil) { //add a closure here to do something when view has rendered
        super.init(frame: frame)
        backgroundColor = UIColor(red:0.00, green:0.18, blue:0.42, alpha:1.0)
        self.completed = completed
        makeShape()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func makeShape(){
        let shapeToFill = CAShapeLayer()
        shapeToFill.backgroundColor = UIColor.gray.cgColor
        shapeToFill.fillColor = UIColor.cyan.cgColor
        shapeToFill.masksToBounds = false
        
        let shapePath = UIBezierPath()
        shapePath.move(to: quickPoint(414, 34))
        shapePath.addCurve(to: quickPoint(121, 89), controlPoint1: quickPoint(325, 17), controlPoint2: quickPoint(200, 6))
        shapePath.addCurve(to: quickPoint(162, 218), controlPoint1: quickPoint(56, 182), controlPoint2: quickPoint(138, 221))
        shapePath.addCurve(to: quickPoint(272, 116), controlPoint1: quickPoint(229, 217), controlPoint2: quickPoint(275, 133))
        shapePath.addCurve(to: quickPoint(258, 112), controlPoint1: quickPoint(273, 110), controlPoint2: quickPoint(265, 104))
        shapePath.addCurve(to: quickPoint(153, 135), controlPoint1: quickPoint(233, 156), controlPoint2: quickPoint(164, 189))
        shapePath.addCurve(to: quickPoint(414, 67), controlPoint1: quickPoint(155, 93), controlPoint2: quickPoint(213, 19))
        shapePath.addLine(to: quickPoint(414, 34))
        shapePath.addLine(to: quickPoint(414, 81))
        shapePath.addCurve(to: quickPoint(328, 159), controlPoint1: quickPoint(396, 88), controlPoint2: quickPoint(361, 119))
        shapePath.addCurve(to: quickPoint(91, 383), controlPoint1: quickPoint(228, 289), controlPoint2: quickPoint(173, 380))
        shapePath.addCurve(to: quickPoint(90, 263), controlPoint1: quickPoint(36, 383), controlPoint2: quickPoint(0, 314))
        shapePath.addCurve(to: quickPoint(71, 240), controlPoint1: quickPoint(96, 259), controlPoint2: quickPoint(99, 241))
        shapePath.addCurve(to: quickPoint(0, 292), controlPoint1: quickPoint(59, 240), controlPoint2: quickPoint(10, 267))
        shapePath.addLine(to: quickPoint(0, 407))
        shapePath.addCurve(to: quickPoint(5, 414), controlPoint1: quickPoint(0, 409), controlPoint2: quickPoint(4, 413))
        shapePath.addCurve(to: quickPoint(183, 414), controlPoint1: quickPoint(29,438), controlPoint2: quickPoint(108,465))
        shapePath.addCurve(to: quickPoint(328, 223), controlPoint1: quickPoint(228, 382), controlPoint2: quickPoint(278, 309))
        shapePath.addCurve(to: quickPoint(414, 116), controlPoint1: quickPoint(352, 185), controlPoint2: quickPoint(370, 153))
        shapePath.close()
        
        shapeToFill.path = shapePath.cgPath
        
        layer.addSublayer(shapeToFill)

        let path = UIBezierPath()
        path.move(to: CGPoint(x: 250, y: 90))
        path.addCurve(to: CGPoint(x: 420, y: 48), controlPoint1: CGPoint(x:130,y:342), controlPoint2: CGPoint(x:-21,y:-34))
        path.move(to: CGPoint(x: 430, y: 92))
        path.addCurve(to: CGPoint(x:198, y:363), controlPoint1: CGPoint(x:339,y:142), controlPoint2: CGPoint(x:269,y:298))
        path.addCurve(to: CGPoint(x:100, y:254), controlPoint1: CGPoint(x:60 ,y:502), controlPoint2: CGPoint(x:-89,y: 335))

        let shapeToDraw = CAShapeLayer()
        shapeToDraw.backgroundColor = UIColor.blue.cgColor
        shapeToDraw.fillColor = UIColor.clear.cgColor
        shapeToDraw.strokeColor = UIColor.white.cgColor
        shapeToDraw.lineWidth = 100
        shapeToDraw.path = path.cgPath
        shapeToDraw.masksToBounds = false
        shapeToDraw.mask = shapeToFill

        layer.addSublayer(shapeToDraw)

        let xScaleFactor =  frame.width / 414
        let yScaleFactor = frame.height / 414

        if xScaleFactor != 1 && yScaleFactor != 1 {
            let scaleTransform = CATransform3DMakeScale(xScaleFactor, yScaleFactor, 1.0)
            if let layers = layer.sublayers {
                layers.forEach { $0.transform = scaleTransform }
            }
        }
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        firstDuration = 2
        animation.duration = firstDuration
        animation.delegate = self
        shapeToDraw.add(animation, forKey: "MyAnimation")
    }
    
    func drawSecond() {
        let secondShapeToFill = CAShapeLayer()
        secondShapeToFill.backgroundColor = UIColor.gray.cgColor
        secondShapeToFill.fillColor = UIColor.cyan.cgColor
        secondShapeToFill.masksToBounds = false
        
        let secondShapePath = UIBezierPath()
        secondShapePath.move(to: quickPoint(414, 188))
        secondShapePath.addCurve(to: quickPoint(208, 224), controlPoint1: quickPoint(310, 193), controlPoint2: quickPoint(241, 200))
        secondShapePath.addCurve(to: quickPoint(224, 240), controlPoint1: quickPoint(190, 237), controlPoint2: quickPoint(207, 258))
        secondShapePath.addCurve(to: quickPoint(275, 223), controlPoint1: quickPoint(225, 239), controlPoint2: quickPoint(241, 228))
        secondShapePath.addCurve(to: quickPoint(414, 208), controlPoint1: quickPoint(317, 218), controlPoint2: quickPoint(365, 212))
        secondShapePath.close()
        
        secondShapeToFill.path = secondShapePath.cgPath
        
        layer.addSublayer(secondShapeToFill)
        
        let secondPath = UIBezierPath()
        secondPath.move(to: CGPoint(x: 190, y: 237))
        secondPath.addCurve(to: CGPoint(x:430, y:196), controlPoint1: CGPoint(x:241 ,y:201), controlPoint2: CGPoint(x:397,y: 203))
        
        let secondShapeToDraw = CAShapeLayer()
        secondShapeToDraw.backgroundColor = UIColor.blue.cgColor
        secondShapeToDraw.fillColor = UIColor.clear.cgColor
        secondShapeToDraw.strokeColor = UIColor.white.cgColor
        secondShapeToDraw.lineWidth = 100
        secondShapeToDraw.path = secondPath.cgPath
        secondShapeToDraw.masksToBounds = false
        secondShapeToDraw.mask = secondShapeToFill
        
        layer.addSublayer(secondShapeToDraw)
        
        let xScaleFactor =  frame.width / 414
        let yScaleFactor = frame.height / 414
        
        if xScaleFactor != 1 && yScaleFactor != 1 {
            let scaleTransform = CATransform3DMakeScale(xScaleFactor, yScaleFactor, 1.0)
            if let layers = layer.sublayers {
                layers.forEach { $0.transform = scaleTransform }
            }
        }
        
        let secondAnimation = CABasicAnimation(keyPath: "strokeEnd")
        secondAnimation.delegate = self
        secondAnimation.fromValue = 0
        secondAnimation.duration = 0.5
        secondShapeToDraw.add(secondAnimation, forKey: "MyAnimation")

    }
    
    func quickPoint(_ x: Int, _ y: Int) -> CGPoint {
        return CGPoint(x: x, y: y)
    }
    
}

extension FordLetterView: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if anim.duration == firstDuration {
            drawSecond()
        } else {
            if let completed = completed {
                completed()
            }
        }
    }
}


