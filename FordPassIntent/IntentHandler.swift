//
//  IntentHandler.swift
//  FordPassIntent
//
//  Created by Gary Riches on 29/06/2018.
//  Copyright © 2018 Bouncing Ball Limited. All rights reserved.
//

import Intents

class IntentHandler: INExtension, ServiceCheckIntentHandling, ExtendReservationIntentHandling {
	
	let SharedDefaults = UserDefaults.init(suiteName: "group.mobi.bouncingball.fordpass")!
    
    override func handler(for intent: INIntent) -> Any {
        // This is the default implementation.  If you want different objects to handle different intents,
        // you can override this and return the handler you want for that particular intent.
        return self
    }
	
	public func handle(intent: ServiceCheckIntent, completion: @escaping (ServiceCheckIntentResponse) -> Void) {
		completion(ServiceCheckIntentResponse(code: .success, userActivity: nil))
	}
	
	public func handle(intent: ExtendReservationIntent, completion: @escaping (ExtendReservationIntentResponse) -> Void) {
        
        completion(ExtendReservationIntentResponse(code: .success, userActivity: nil))
	}
	
	public func confirm(intent: ExtendReservationIntent, completion: @escaping (ExtendReservationIntentResponse) -> Void) {
        let response = ExtendReservationIntentResponse(code: .checking, userActivity: nil)
        response.date = ""
        
        
        let hours = intent.hours?.intValue ?? 0
        let minutes = intent.minutes?.intValue ?? 0
        if let date = SharedDefaults.value(forKey: "reservationDate") as? Date {
            let newDate = date.addingTimeInterval(Double((hours / 2) * 60 * 60) + Double((minutes / 2) * 60))
            let displayDate = date.addingTimeInterval(Double(hours * 60 * 60) + Double(minutes * 60))
            SharedDefaults.set(newDate, forKey: "reservationDate")

            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .short
            response.date = dateFormatter.string(from: displayDate)
        }
        
		completion(response)
	}
}

extension IntentHandler: QueryReservationIntentHandling {
    public func handle(intent: QueryReservationIntent, completion: @escaping (QueryReservationIntentResponse) -> Void) {
        
        let response = QueryReservationIntentResponse(code: .success, userActivity: nil)
        response.date = ""

        if let date = SharedDefaults.value(forKey: "reservationDate") as? Date {

            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .short
            response.date = dateFormatter.string(from: date)
        }

        completion(response)
    }
}

extension IntentHandler: SearchIntentHandling {
    
    func handle(intent: SearchIntent, completion: @escaping (SearchIntentResponse) -> Void) {
        let activity = NSUserActivity(activityType: "mobi.bouncingball.FordPassHackathon.search")
        activity.title = "Search for " + (intent.findable ?? "")
        if let findable = intent.findable {
            activity.userInfo = ["findable" : findable]
        }
        completion(SearchIntentResponse(code: .continueInApp, userActivity: activity))
    }
    
}
