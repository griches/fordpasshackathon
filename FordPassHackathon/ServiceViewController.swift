//
//  ServiceViewController.swift
//  FordPassHackathon
//
//  Created by Gary Riches on 29/06/2018.
//  Copyright © 2018 Bouncing Ball Limited. All rights reserved.
//

import Foundation
import UIKit
import IntentsUI

class ServiceViewController: UIViewController {

}

extension ServiceViewController: INUIHostedViewControlling {
	
	/// Prepare your view controller for displaying the details of the intent
	func configureView(for parameters: Set<INParameter>,
					   of interaction: INInteraction,
					   interactiveBehavior: INUIInteractiveBehavior,
					   context: INUIHostedViewContext,
					   completion: @escaping (Bool, Set<INParameter>, CGSize) -> Void) {
		
		guard let intent = interaction.intent as? ServiceCheckIntent else {
			completion(false, Set(), .zero)
			return
		}
		
		for view in view.subviews {
			view.removeFromSuperview()
		}
		
		var desiredSize = CGSize.zero
		desiredSize.width = self.extensionContext?.hostedViewMaximumAllowedSize.width ?? 320
		desiredSize.height = 180
		
		completion(true, parameters, desiredSize)
	}
}
