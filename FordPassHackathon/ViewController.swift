//
//  ViewController.swift
//  FordPassHackathon
//
//  Created by Gary Riches on 29/06/2018.
//  Copyright © 2018 Bouncing Ball Limited. All rights reserved.
//

import UIKit
import Intents
import os.log

class ViewController: UIViewController {
	
	let SharedDefaults = UserDefaults.init(suiteName: "group.mobi.bouncingball.fordpass")!

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		
        let date = Date().addingTimeInterval(60 * 60 * 24)
        SharedDefaults.set(date, forKey: "reservationDate")
        
        let startDate = Date().addingTimeInterval(60 * 60 * 22)
        SharedDefaults.set(startDate, forKey: "reservationStartDate")
		
		donateInteractions()
	}
}

// Intents
extension ViewController {
	
	// Siri shortcuts
	private func donateInteractions() {
		let minutesIntent = ExtendReservationIntent()
		minutesIntent.minutes = 30
		var interaction = INInteraction(intent: minutesIntent, response: nil)
		interaction.donate { (error) in
			if error != nil {
				if let error = error as NSError? {
					os_log("Interaction donation failed: %@", log: OSLog.default, type: .error, error)
				}
			} else {
				os_log("Successfully donated interaction")
			}
		}
		
//        let hoursIntent = ExtendReservationIntent()
//        hoursIntent.hours = 1
//        interaction = INInteraction(intent: hoursIntent, response: nil)
//        interaction.donate { (error) in
//            if error != nil {
//                if let error = error as NSError? {
//                    os_log("Interaction donation failed: %@", log: OSLog.default, type: .error, error)
//                }
//            } else {
//                os_log("Successfully donated interaction")
//            }
//        }
        
        let queryIntent = QueryReservationIntent()
        interaction = INInteraction(intent: queryIntent, response: nil)
        interaction.donate { (error) in
            if error != nil {
                if let error = error as NSError? {
                    os_log("Interaction donation failed: %@", log: OSLog.default, type: .error, error)
                }
            } else {
                os_log("Successfully donated interaction")
            }
        }
		
		let serviceIntent = ServiceCheckIntent()
		interaction = INInteraction(intent: serviceIntent, response: nil)
		interaction.donate { (error) in
			if error != nil {
				if let error = error as NSError? {
					os_log("Interaction donation failed: %@", log: OSLog.default, type: .error, error)
				}
			} else {
				os_log("Successfully donated interaction")
			}
		}
        
        registerSearchIntents(for: [
            "coffee",
            "food",
            "fuel",
            "park"
            ])
	}
    
    func registerSearchIntents(for findables: [String]) {
        findables.forEach { findable in
            let searchIntent = SearchIntent()
            searchIntent.findable = findable
            let interaction = INInteraction(intent: searchIntent, response: nil)
            interaction.donate { error in
                guard let error = error else {
                    os_log("Successfully donated interaction")
                    return
                }
                os_log("Interaction donation failed: %@", log: OSLog.default, type: .error, error as NSError)
            }
        }
    }
}

